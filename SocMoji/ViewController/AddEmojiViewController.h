//
//  AddNewViewController.h
//  SocMoji
//
//  Created by a on 4/9/15.
//  Copyright (c) 2015 Mathrawk, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuperViewController.h"
#import "EmojiCell.h"

@interface AddEmojiViewController : SuperViewController

@end
