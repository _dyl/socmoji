//
//  EmojiCollectionView.m
//  SocMoji
//
//  Created by b on 4/16/15.
//  Copyright (c) 2015 Mathrawk, LLC. All rights reserved.
//

#import "EmojiCollectionView.h"
#import "EmojiCell.h"
#import "GlobalFunc.h"

@interface EmojiCollectionView()
@property (strong, nonatomic) NSArray *emojis;

@end

@implementation EmojiCollectionView

@synthesize emjDelegate;

-(id) initWithFrame:(CGRect)frame collectionViewLayout:(UICollectionViewLayout *)layout
{
    self = [super initWithFrame:frame collectionViewLayout:layout];
    
    if (self)
    {
		self.emojis = @[@"Aguerro", @"Bale", @"Balotteli", @"Benteke", @"Benzema", @"Berahino",
						@"Borini", @"Cavani", @"Costa", @"Coutinho", @"Drogba", @"Dzeko", @"Eriksen",
						@"Fellaini", @"Gerrard", @"Hazard", @"Ibrahimovic", @"Iniesta", @"Kane",
						@"Klose", @"Lahm", @"Lampard", @"Lewandowski", @"Luca", @"Luiz", @"Lukaku",
						@"Mauro", @"Messi", @"Modric", @"Neymar", @"Ozil", @"Pepe", @"Pirlo", @"Pogba",
						@"Ramos", @"Ribery",  @"Robben", @"Ronaldo", @"Rooney", @"Sanchez", @"Silva",
						@"Skrtel", @"Sturridge", @"Suarez", @"Torry", @"Toure", @"Tevez", @"Toure",
						@"Van Persie", @"Welbeck", @"Xavi"];
		
        self.delegate = self;
        self.dataSource = self;
    }
    [self setBackgroundColor:[UIColor whiteColor]];
    return self;
}

- (void) setSoccerEmojis : (NSArray *)emojis
{
    self.emojis = emojis;
    [self reloadData];
}

#pragma mark - CollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.emojis count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    EmojiCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    [cell setBackgroundColor:[UIColor grayColor]];
    [cell setEmoji:[self.emojis objectAtIndex:indexPath.row]];
    
    [collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([GlobalFunc isPad])
        return CGSizeMake(192, 224);
    else
        return CGSizeMake(96, 112);
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (emjDelegate && [emjDelegate respondsToSelector:@selector(EmojiCollectionView:didSelectItemAtIndexPath:)]){
        [emjDelegate EmojiCollectionView:self didSelectItemAtIndexPath:indexPath];
    }
        
}


@end
