//
//  EmojiCollectionView.h
//  SocMoji
//
//  Created by b on 4/16/15.
//  Copyright (c) 2015 Mathrawk, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EmojiCollectionDelegate;

@interface EmojiCollectionView : UICollectionView<UICollectionViewDataSource, UICollectionViewDelegate>

@property(nonatomic,assign) id<EmojiCollectionDelegate> emjDelegate;

- (void) setSoccerEmojis : (NSArray *)emojis;
@end

@protocol EmojiCollectionDelegate<NSObject>

@optional
- (void) EmojiCollectionView:(EmojiCollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;

@end