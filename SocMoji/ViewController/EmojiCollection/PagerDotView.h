//
//  PagerDotView.h
//  SocMoji
//
//  Created by b on 4/17/15.
//  Copyright (c) 2015 Mathrawk, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TAAbstractDotView.h"

@interface PagerDotView : TAAbstractDotView

@end
