//
//  AllEmojiViewController.m
//  SocMoji
//
//  Created by a on 4/9/15.
//  Copyright (c) 2015 Mathrawk, LLC. All rights reserved.
//

#import "FavoriteViewController.h"
#import "EmojiCollectionView.h"

@interface FavoriteViewController () <UIScrollViewDelegate, EmojiCollectionDelegate>

@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (strong, nonatomic) NSArray *emojis;

@end

@implementation FavoriteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.emojis = @[@"Bale", @"Balotteli", @"Costa", @"Messi", @"Neymar", @"Ozil", @"Ronaldo", @"Rooney", @"Sturridge"];
}

- (void)viewDidAppear:(BOOL)animated {
    [self.navigationItem setTitle:@"Favourite"];
    [self setupEmojis];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

#pragma mark - ScrollView delegate

- (void)setupEmojis
{
    CGRect frame = CGRectMake(0, 0, CGRectGetWidth(self.vwContent.frame), CGRectGetHeight(self.vwContent.frame));

    EmojiCollectionView *emjCollection = [[EmojiCollectionView alloc] initWithFrame:frame collectionViewLayout:[[UICollectionViewFlowLayout alloc] init]];
    [emjCollection setSoccerEmojis:self.emojis];
    emjCollection.emjDelegate = self;

    UINib *cellNib = [UINib nibWithNibName:@"EmojiCell" bundle:nil];
    [emjCollection registerNib:cellNib forCellWithReuseIdentifier:@"cell"];

    [self.vwContent addSubview:emjCollection];
}

#pragma mark - EmojiCollectionViewDelegate
- (void) EmojiCollectionView:(EmojiCollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *message = [self.emojis objectAtIndex:indexPath.row];
    UIImage *image = [UIImage imageNamed:message];
    NSArray *activityItems = [NSArray arrayWithObjects: message, image , nil];
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    [self presentViewController:activityController animated:YES completion:nil];
}

@end
