//
//  AddNewViewController.m
//  SocMoji
//
//  Created by a on 4/9/15.
//  Copyright (c) 2015 Mathrawk, LLC. All rights reserved.
//

#import "AddEmojiViewController.h"
#import "TAPageControl.h"
#import "EmojiCollectionView.h"
#import "PagerDotView.h"

@interface AddEmojiViewController () <UIScrollViewDelegate, TAPageControlDelegate>

@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet UIScrollView *scrView;

@property (strong, nonatomic) TAPageControl *emojiPage;
@property (strong, nonatomic) NSArray *emojis;

@end

@implementation AddEmojiViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.emojis = @[@"Balotelli.png", @"Cristiano.png", @"Costa.png"];

    [self setupScrollViewImages];
    self.scrView.delegate = self;

    // Progammatically init a TAPageControl with a custom dot view.
    CGRect frame = CGRectMake(0, CGRectGetHeight(self.vwContent.frame) - 180, CGRectGetWidth(self.vwContent.frame), 40);
    self.emojiPage               = [[TAPageControl alloc] initWithFrame:frame];
    // Example for touch bullet event
    self.emojiPage.delegate      = self;
    self.emojiPage.numberOfPages = self.emojis.count;
    // Custom dot view
    self.emojiPage.dotViewClass  = [PagerDotView class];
    self.emojiPage.dotSize       = CGSizeMake(32, 32);
    [self.vwContent addSubview:self.emojiPage];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    self.scrView.contentSize = CGSizeMake(CGRectGetWidth(self.scrView.frame) * self.emojis.count,
                                          CGRectGetHeight(self.scrView.frame));
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - ScrollView delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSInteger pageIndex = scrollView.contentOffset.x / CGRectGetWidth(scrollView.frame);
    
    self.emojiPage.currentPage = pageIndex;
}


// Example of use of delegate for second scroll view to respond to bullet touch event
- (void)TAPageControl:(TAPageControl *)pageControl didSelectPageAtIndex:(NSInteger)index
{
    NSLog(@"Bullet index %ld", (long)index);
    [self.scrView scrollRectToVisible:CGRectMake(CGRectGetWidth(self.scrView.frame) * index, 0, CGRectGetWidth(self.scrView.frame), CGRectGetHeight(self.scrView.frame)) animated:YES];
}

#pragma mark - Utils
- (void)setupScrollViewImages
{
    [self.vwContent layoutIfNeeded];
    [self.emojis enumerateObjectsUsingBlock:^(NSString *imageName, NSUInteger idx, BOOL *stop) {
        CGRect frame = self.scrView.frame;
        frame.origin.x = self.scrView.frame.size.width * idx;
        
        EmojiCollectionView *emjCollection = [[EmojiCollectionView alloc] initWithFrame:frame collectionViewLayout:[[UICollectionViewFlowLayout alloc] init]];
        
        UINib *cellNib = [UINib nibWithNibName:@"EmojiCell" bundle:nil];
        [emjCollection registerNib:cellNib forCellWithReuseIdentifier:@"cell"];

        [self.scrView addSubview:emjCollection];
    }];
}

@end

