//
//  TabViewController.m
//  SocMoji
//
//  Created by a on 4/9/15.
//  Copyright (c) 2015 Mathrawk, LLC. All rights reserved.
//

#import "TabViewController.h"
#import "SettingViewController.h"

@interface TabViewController ()

@end

@implementation TabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"setting.png"] landscapeImagePhone:[UIImage imageNamed:@"setting.png"]  style:UIBarButtonItemStyleDone                                            target:self action:@selector(onTapSettings:)];
//    self.navigationItem.rightBarButtonItem = button;
    
    self.tabBar.translucent = NO;
    self.selectedIndex = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) onTapSettings : (id)sender
{
    SettingViewController *setting = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SettingViewController"];
    
    [self.navigationController pushViewController:setting animated:YES];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
