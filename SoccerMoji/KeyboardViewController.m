//
//  KeyboardViewController.m
//  SoccerMoji
//
//  Created by b on 4/17/15.
//  Copyright (c) 2015 Mathrawk, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyboardViewController.h"
#import "Toast+UIView.h"
#import "EmojiCell.h"

@interface KeyboardViewController ()
{
    id<UITextDocumentProxy> keyProxy;
    
    IBOutletCollection(UIButton) NSArray *buttons;
    
}
@property (weak, nonatomic) IBOutlet UICollectionView *collections;
@property (strong, nonatomic) NSArray *emojis;
@end

@implementation KeyboardViewController

-(id) init
{
    self = [super init];
    if (!self) return nil;
    
	self.emojis = @[@"Aguerro", @"Bale", @"Balotteli", @"Benteke", @"Benzema", @"Berahino",
					@"Borini", @"Cavani", @"Costa", @"Coutinho", @"Drogba", @"Dzeko", @"Eriksen",
					@"Fellaini", @"Gerrard", @"Hazard", @"Ibrahimovic", @"Iniesta", @"Kane",
					@"Klose", @"Lahm", @"Lampard", @"Lewandowski", @"Luca", @"Luiz", @"Lukaku",
					@"Mauro", @"Messi", @"Modric", @"Neymar", @"Ozil", @"Pepe", @"Pirlo", @"Pogba",
					@"Ramos", @"Ribery",  @"Robben", @"Ronaldo", @"Rooney", @"Sanchez", @"Silva",
					@"Skrtel", @"Sturridge", @"Suarez", @"Torry", @"Toure", @"Tevez", @"Toure",
					@"Van Persie", @"Welbeck", @"Xavi"];

    return self;
}

- (void)updateViewConstraints {
    [super updateViewConstraints];
    
    // Add custom view sizing constraints here
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    keyProxy = self.textDocumentProxy;
   
    UINib *kbdNib = [UINib nibWithNibName:@"KeyboardView" bundle:nil];
    NSArray *objs = [kbdNib instantiateWithOwner:self options:nil];
    UIView *view = (UIView*)[objs objectAtIndex:1];
    
    [self layoutEmojis];
    [self.view layoutIfNeeded];
    view.frame = self.view.frame;
    [self.view addSubview:view];
    [self.view layoutIfNeeded];
    
    if (self.collections)
    {
        UINib *cellNib = [UINib nibWithNibName:@"EmojiCell" bundle:nil];
        [self.collections registerNib:cellNib forCellWithReuseIdentifier:@"cell"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated
}

- (void)textWillChange:(id<UITextInput>)textInput {
    // The app is about to change the document's contents. Perform any preparation here.
}

- (void)textDidChange:(id<UITextInput>)textInput {
    // The app has just changed the document's contents, the document context has been updated.
    
    UIColor *textColor = nil;
    if (self.textDocumentProxy.keyboardAppearance == UIKeyboardAppearanceDark) {
        textColor = [UIColor whiteColor];
    } else {
        textColor = [UIColor blackColor];
    }
}

- (IBAction)onTouchKey:(id)sender {
    UIButton *button = (UIButton*)sender;
    
    UIImage *img = [UIImage imageNamed:[self.emojis objectAtIndex:button.tag]];
    do
    {
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        if (!pasteboard) break;
        
        NSData *imgData = UIImagePNGRepresentation(img);
        if (!imgData) break;
        [pasteboard setData:imgData forPasteboardType:@"public.png"];
        
        NSString *str = [pasteboard string];
        if (!str) break;
        [self.textDocumentProxy insertText:str];
    }while(NO);
    [self.view makeToast:@"SoccerMoji copied, Now paste it" duration:2 position:@"center"];
}

- (IBAction) onTouchSpace:(id)sender
{
    [keyProxy insertText:@" "];
}

- (IBAction) onTouchBackspace:(id)sender
{
    [keyProxy deleteBackward];
}

- (IBAction) onTouchEnter:(id)sender
{
    [keyProxy insertText:@"\n\a"];
}

- (IBAction) onTouchDismiss:(id)sender
{
    [self dismissKeyboard];
}

- (IBAction)onNextKeyboard:(id)sender {
    [self advanceToNextInputMode];
}

- (void) layoutEmojis
{
    NSUInteger count = self.emojis.count;
    
    [buttons enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL *stop)
	{

		NSString *name = [self.emojis objectAtIndex:idx];
		NSString *fullname = @"icon_";
		[fullname stringByAppendingString:name];
    	if (idx < count)
       		[button setBackgroundImage:[UIImage imageNamed:name] forState:UIControlStateNormal];
		//else
			//    [button setEnabled:NO];
    }];

}

#pragma mark - CollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.emojis count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    EmojiCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    [cell setBackgroundColor:[UIColor grayColor]];
    [cell setEmoji:[self.emojis objectAtIndex:indexPath.row]];
    
    [collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGRect rect = self.collections.frame;
    
    CGFloat height = (CGRectGetHeight(rect) - 10) / 2;
    CGFloat width = height * 0.8f;
    return CGSizeMake(width, height);
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIImage *img = [UIImage imageNamed:[self.emojis objectAtIndex:indexPath.row]];
    do
    {
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        if (!pasteboard) break;
        
        NSData *imgData = UIImagePNGRepresentation(img);
        if (!imgData) break;
        [pasteboard setData:imgData forPasteboardType:@"public.png"];
        
        NSString *str = [pasteboard string];
        if (!str) break;
        [self.textDocumentProxy insertText:str];
    }while(NO);
    [self.view makeToast:@"SoccerMoji copied, Now paste it" duration:2 position:@"center"];
}
@end
