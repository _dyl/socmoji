//
//  EmojiCell.h
//  SocMoji
//
//  Created by b on 4/16/15.
//  Copyright (c) 2015 Mathrawk, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmojiCell : UICollectionViewCell
-(void) setEmoji:(NSString*)name;
@end
