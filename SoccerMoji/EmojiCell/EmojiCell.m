//
//  EmojiCell.m
//  SocMoji
//
//  Created by b on 4/16/15.
//  Copyright (c) 2015 Mathrawk, LLC. All rights reserved.
//

#import "EmojiCell.h"

@interface EmojiCell()
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIImageView *vwEmoji;

@end

@implementation EmojiCell


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

-(void) setEmoji:(NSString*)name
{
    [_name setText:name];
    UIImage *emoj = [UIImage imageNamed:name];
    [_vwEmoji setImage:emoj];
}
@end
